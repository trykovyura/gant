package ru.bazon.vaadin.ganttdiagram.demo;

import ru.bazon.vaadin.ganttdiagram.GanttDiagram;
import ru.bazon.vaadin.ganttdiagram.GanttDiagramCellStyleGeneratorAdapter;
import ru.bazon.vaadin.ganttdiagram.model.GANTTCOLUMN;
import ru.bazon.vaadin.ganttdiagram.model.GanttTask;

public class GanttDiagramDemoCellStyleGenerator extends GanttDiagramCellStyleGeneratorAdapter 
{

	@Override
	public String getRowStyle(GanttDiagram diagram, GanttTask task)
	{
		if (task.getParameter("name").equals("дыфлвоа"))
		{
			return "color-green";
		}
		else if (task.getParameter("name").equals("task12"))
		{
			return "background-pink";
		}
		
		return super.getRowStyle(diagram, task);
	}
	
	@Override
	public String getCellStyle(GanttDiagram diagram, GanttTask task, GANTTCOLUMN column)
	{
		if (column == GANTTCOLUMN.COMPLETE)
		{
			return "background-red v-table-cell-content-font-bold";
		}
		
		return super.getCellStyle(diagram, task, column);
	}

	@Override
	public String getCellStyle(GanttDiagram diagram, GanttTask task, String column)
	{
		if (column.equals("overflow") && task.getCompleteState() == 100)
		{
			return "font-bold";
		}

		return super.getCellStyle(diagram, task, column);
	}

	@Override
	public String getDiagramCellStyle(GanttDiagram diagram, GanttTask task)
	{
		return super.getDiagramCellStyle(diagram, task);
	}

	@Override
	public String getDiagramTaskStyle(GanttDiagram diagram, GanttTask task)
	{
		return super.getDiagramTaskStyle(diagram, task);
	}

	@Override
	public String getDiagramTaskCompleteStyle(GanttDiagram diagram, GanttTask task)
	{
		return super.getDiagramTaskCompleteStyle(diagram, task);
	}

}
