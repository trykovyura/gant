package ru.bazon.vaadin.ganttdiagram.demo;

import java.text.DecimalFormat;
import java.util.Arrays;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ru.bazon.vaadin.ganttdiagram.GanttColumnFormatter;
import ru.bazon.vaadin.ganttdiagram.GanttDiagram;
import ru.bazon.vaadin.ganttdiagram.GanttGeneratedColumn;
import ru.bazon.vaadin.ganttdiagram.model.GANTTCOLUMN;
import ru.bazon.vaadin.ganttdiagram.model.GANTTDIAGRAMSCALE;
import ru.bazon.vaadin.ganttdiagram.model.GanttDependencyType;
import ru.bazon.vaadin.ganttdiagram.model.GanttDiagramModel;
import ru.bazon.vaadin.ganttdiagram.model.GanttTask;
import ru.bazon.vaadin.ganttdiagram.model.GanttTaskControlPoint;
import ru.bazon.vaadin.ganttdiagram.model.GanttTaskGeneratedColumnDescription;
import ru.bazon.vaadin.ganttdiagram.model.GanttTaskParameterDescription;
import ru.bazon.vaadin.ganttdiagram.taskediting.ComboBoxGanttFieldEditor;
import ru.bazon.vaadin.ganttdiagram.treetable.GanttTreeTable;
import ru.bazon.vaadin.ganttdiagram.utils.GJodaUtils;

import com.vaadin.Application;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class GanttDiagramDemoApplication extends Application
{

	public static DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm:ss");
	public static DateTimeFormatter dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy");
	
	@Override
	public void init()
	{
		setTheme("vtmtheme");
		
		Window mainWindow = new Window("Диаграмма Ганта");
		mainWindow.setSizeFull();
		
		GanttDiagramModel gdata = new GanttDiagramModel();
		
		gdata.setGanttColumnVisible(GANTTCOLUMN.STARTTIME, "Время начала", null, null);
		gdata.setGanttColumnVisible(GANTTCOLUMN.ENDTIME, "Время окончания", null, null);		
		gdata.setGanttColumnVisible(GANTTCOLUMN.DURATION, "Продолжительность", null, null);
		gdata.setGanttColumnVisible(GANTTCOLUMN.COMPLETE, "Выполнено", null, null);
		
		gdata.setTaskTooltipTemplate("<b>Name :</b> ${name}<br><b>Start time:</b> ${startTime}<br><b>End time:</b> ${endTime}");
		gdata.addParametersDescription(new GanttTaskParameterDescription("name", String.class, "", "Наименование"));
		gdata.addParametersDescription(new GanttTaskParameterDescription("additionalParam", Double.class, "", true));
		gdata.addParametersDescription(new GanttTaskParameterDescription("stringParam", String.class, "", "Строка"));
		
		gdata.setColumnFormatter(GANTTCOLUMN.COMPLETE.getId(), new GanttColumnFormatter<Double>()
		{
			public String formatValue(String columnName, Double value)
			{			
				DecimalFormat df = new DecimalFormat("###");
				return df.format(value);
			}
		});
		
		gdata.setColumnFieldEditor("stringParam", new ComboBoxGanttFieldEditor(Arrays.asList(new String[] { "бла", "угу", "параметер", "гы гы" })));
		
		gdata.addGanttGeneratedColumn(new GanttTaskGeneratedColumnDescription(
			"generatedColumn", 
			new GanttGeneratedColumn()
			{
				@Override
				public Object generateCell(GanttTreeTable source, GanttTask task, String columnId)
				{
					return task.getCompleteState() - 12;
				}
			}, 
			"Сгенерированный столбец"));
		
		gdata.addGanttGeneratedColumn(new GanttTaskGeneratedColumnDescription(
				"overflow", 
				new GanttGeneratedColumn()
				{
					@Override
					public Object generateCell(GanttTreeTable treeTable, GanttTask task, String columnId)
					{
						DateTime now = new DateTime();
						String value;
						if (now.isAfter(task.getEndTime()))
						{
							value = String.valueOf(GJodaUtils.unitBetween(task.getEndTime(), now, treeTable.getDiagram().getScale().getGanttDiagramPeriod()));
						}
						else
						{
							value = "-";
						}
						
						return new Label(value);
					}
				}, 
				"Просрочено"));

		gdata.setColumnsOrder(Arrays.asList(new Object[] {"name", "overflow", "generatedColumn", GANTTCOLUMN.COMPLETE, GANTTCOLUMN.STARTTIME, GANTTCOLUMN.ENDTIME, "stringParam" }));

		GanttTask task2 = new GanttTask(dateFormat.parseDateTime("10.07.2011"), dateFormat.parseDateTime("20.12.2011"), 4);		
		task2.addParameter("name", "task with very long name. long name long name long name long name");
		task2.setAutoCalculation(false);

		GanttTask task3 = new GanttTask(dateFormat.parseDateTime("10.07.2011"), dateFormat.parseDateTime("25.12.2011"), 60, task2);		
		task3.addParameter("name", "Задача с вехой");		
		task3.addParameter("stringParam", "параметер");
		task3.addControlPoint(new GanttTaskControlPoint(
				dateTimeFormat.parseDateTime("12.10.2011 8:00:00"),
				"Веха истории: \"${value, 'dd.MM.yyyy'}\""));

		gdata.addTask(task2);

		addWeekTasks(gdata);
		addMonthTasks(gdata);

		GanttDiagram diagram = new GanttDiagram(gdata);
		
		diagram.setGanttDiagramCellStyleGenerator(new GanttDiagramDemoCellStyleGenerator());
		
		diagram.addAvailableScale(GANTTDIAGRAMSCALE.DAY, "День недели");
		diagram.addAvailableScale(GANTTDIAGRAMSCALE.DAY_MONTH, "День месяца");
		diagram.addAvailableScale(GANTTDIAGRAMSCALE.WEEK, "Неделя");
		diagram.addAvailableScale(GANTTDIAGRAMSCALE.MONTH, "Месяц");
		diagram.addAvailableScale(GANTTDIAGRAMSCALE.YEAR, "Год");
		
		//diagram.setFilterColumn("name");
		
		diagram.setTaskEditWindowCaption("Редактирование задачи");
		diagram.setTaskEditSaveChangeButtonCaption("Сохранить");
		
		diagram.setWidth("100%");
		diagram.setHeight("100%");
		
		diagram.setScale(GANTTDIAGRAMSCALE.MONTH);
		
		VerticalLayout vt = new VerticalLayout(); 
		vt.addComponent(diagram);        
        vt.addComponent(new Label("test"));
        
        vt.setHeight("100%");
        vt.setExpandRatio(diagram, 10000);
        
        mainWindow.setContent(vt);
        
        setMainWindow(mainWindow);
	}

	private void addHoursTasks(GanttDiagramModel gdata)
	{
		GanttTask parentTask = new GanttTask();
		parentTask.addParameter("name", "ParentTask");

		GanttTask task1 = new GanttTask(parentTask);
		task1.addParameter("name", "task1");
		task1.addParameter("additionalParam", 40d);

		GanttTask task11 = new GanttTask(dateTimeFormat.parseDateTime("24.10.2011 8:20:33"), dateTimeFormat.parseDateTime("25.10.2011 20:41:33"), 40, task1);
		task11.addParameter("name", "task11");
		
		GanttTask task12 = new GanttTask(dateTimeFormat.parseDateTime("20.10.2011 8:40:33"), dateTimeFormat.parseDateTime("20.10.2011 8:50:33"), 40, task1);
		task12.addParameter("name", "task12");
		task12.addParameter("additionalParam", 15d);

		GanttTask task2 = new GanttTask(dateTimeFormat.parseDateTime("24.10.2011 8:00:10"), dateTimeFormat.parseDateTime("24.10.2011 8:00:33"), 40, task1);
		task2.addParameter("name", "task2");
		
		gdata.addTask(parentTask);
		
		gdata.addDependency(GanttDependencyType.END_BEGIN, task11, task2);
	}

	private void addWeekTasks(GanttDiagramModel gdata)
	{
		GanttTask parentTask = new GanttTask();
		parentTask.addParameter("name", "Родительское задача");
		
		GanttTask task1 = new GanttTask(parentTask);
		task1.addParameter("name", "Задача1");
		task1.addParameter("additionalParam", 40d);

		GanttTask task11 = new GanttTask(dateTimeFormat.parseDateTime("24.01.2011 8:01:33"), dateTimeFormat.parseDateTime("31.1.2011 20:01:33"), 40, task1);
		task11.addParameter("name", "Задача11");
		
		GanttTask task2 = new GanttTask(dateFormat.parseDateTime("10.10.2011"), dateFormat.parseDateTime("23.10.2011"), 30, parentTask);
		task2.addParameter("name", "Задача2");
		task2.addParameter("additionalParam", 20d);

		GanttTask task21 = new GanttTask(dateFormat.parseDateTime("13.10.2011"), dateFormat.parseDateTime("16.10.2011"), 100, parentTask);		
		task21.addParameter("name", "Задача22");

		GanttTask task3 = new GanttTask(parentTask);		
		task3.addParameter("name", "Задача3");

		GanttTask task4 = new GanttTask(parentTask);		
		task4.addParameter("name", "Задача4");

		parentTask.setCollapsed(true);

		gdata.addTask(parentTask);
		
		gdata.addDependency(GanttDependencyType.END_BEGIN, task11, task2);
	}

	private void addMonthTasks(GanttDiagramModel gdata)
	{
		GanttTask parentTask = new GanttTask();
		parentTask.addParameter("name", "ParentTask");

		GanttTask task1 = new GanttTask(parentTask);
		task1.addParameter("name", "task1");
		task1.addParameter("additionalParam", 40d);


		GanttTask task11 = new GanttTask(dateTimeFormat.parseDateTime("01.09.2011 8:01:33"), dateTimeFormat.parseDateTime("01.09.2011 20:01:33"), 40, task1);
		task11.addParameter("name", "task11");
		
		GanttTask task12 = new GanttTask(dateFormat.parseDateTime("15.8.2011"), dateFormat.parseDateTime("15.9.2011"), 30, task1);
		task12.addParameter("name", "task12");
		task12.addParameter("additionalParam", 15d);

		GanttTask task2 = new GanttTask(dateFormat.parseDateTime("10.07.2011"), dateFormat.parseDateTime("20.12.2011"), 100, parentTask);		
		task2.addParameter("name", "task2");
		
		gdata.addTask(parentTask);
		
		gdata.addDependency(GanttDependencyType.END_BEGIN, task11, task2);
	}

	private void addYearTasks(GanttDiagramModel gdata)
	{
		GanttTask parentTask = new GanttTask();
		parentTask.addParameter("name", "ParentTask");

		GanttTask task1 = new GanttTask(parentTask);
		task1.addParameter("name", "task1");
		task1.addParameter("additionalParam", 40d);


		GanttTask task11 = new GanttTask(dateTimeFormat.parseDateTime("01.09.2010 8:01:33"), dateTimeFormat.parseDateTime("01.09.2012 20:01:33"), 40, task1);
		task11.addParameter("name", "task11");
		
		GanttTask task12 = new GanttTask(dateFormat.parseDateTime("15.8.2009"), dateFormat.parseDateTime("15.9.2015"), 30, task1);
		task12.addParameter("name", "task12");
		task12.addParameter("additionalParam", 15d);

		GanttTask task2 = new GanttTask(dateFormat.parseDateTime("1.01.2011"), dateFormat.parseDateTime("20.12.2011"), 100, parentTask);		
		task2.addParameter("name", "task2");

		GanttTask task3 = new GanttTask(dateFormat.parseDateTime("10.04.2011"), dateFormat.parseDateTime("20.5.2011"), 27, parentTask);		
		task3.addParameter("name", "task3");

		GanttTask task4 = new GanttTask(dateFormat.parseDateTime("1.04.2011"), dateFormat.parseDateTime("2.4.2011"), 27, parentTask);		
		task4.addParameter("name", "task4");

		gdata.addTask(parentTask);
		
		gdata.addDependency(GanttDependencyType.END_BEGIN, task11, task2);
	}

}
